// 自己呼叫自己本身的執行方法
(function() {

	// 建立底層視窗
	var self = Ti.UI.createWindow({
		'backgroundColor' : 'white' // 背景顏色
	});
	
	if( Titanium.Platform.osname === 'iphone' ){
		// 建立顯示的 Window 視窗
		var win = Ti.UI.createWindow({
			'title': L('title_home'), // 視窗標題
			'backgroundColor':'#FFFFFF' // 背景顏色
		});
		
		// 建立頂部元件
		var nav = Ti.UI.iPhone.createNavigationGroup({
			'window': win // 此元件屬於的 Window 視窗
		});
	
		// 將頂部元件放入底層 Window 視窗
		self.add(nav);
	} else {
		// 按下返回是否關閉視窗(Android)
		self.exitOnClose = false;
	}

	// 建立垂直表視圖
	var tableView = Ti.UI.createTableView();

	// 建立用戶端呼叫方法
	var jsonHTTPRequest = Ti.Network.createHTTPClient();

	// 與伺服器要求成功
	jsonHTTPRequest.onload = function(data) {

		// 接收回傳資料
		var dom = data.source.responseText;
		
		// 解析Json資料
		var json = JSON.parse(dom);
		
		// 產生一個陣列，稍後存放要顯示在TableView的資料
		var rows = [];

		for (var row in json) {
			// 每次解析一筆資料放入rows，每一列資料都可以當作是一個TableViewRow
			rows.push({
				'id' : json[row].ID,
				'title' : json[row].post_title, // 標題
				'color' : 'black', // 文字顏色
				'post_content' : json[row].post_content,
				'guid' : json[row].guid,
				'comment_count' : json[row].comment_count,
				'hasChild' : true	// 顯示右方箭頭
			});
		}
		
		// 將解析完畢的資料陣列給予TableView
		tableView.data = rows;

		// TableView 點擊觸發事件
		tableView.addEventListener('click', function(event) {
			// 建立 Window 子層視窗
			var win2 = Ti.UI.createWindow({
				'backgroundColor' : 'white' // 背景顏色
			});

			// 網頁瀏覽視窗元件
			var web = Ti.UI.createWebView({
				'html' : event.rowData.post_content // HTML代碼
			});

			// 將網頁瀏覽元件放入Window視窗
			win2.add(web);
			
			if( Titanium.Platform.osname === 'iphone' ){
				// 使用頂部元件開啟視窗
				nav.open(win2);
			} else {
				// 按下返回是否關閉視窗(Android)
				win2.exitOnClose = false;
				
				// 開啟Window視窗
				win2.open();
			}
		});
	};

	// 設定呼叫方法與位址
	jsonHTTPRequest.open('GET', 'http://192.168.2.104/barryblog/json-feed.php?action=posts&json=1');

	// 對伺服器發送請求
	jsonHTTPRequest.send();

	// 將UI元件放入顯示的 Window 視窗
	if( Titanium.Platform.osname === 'iphone' ){
		win.add(tableView);
	} else {
		self.add(tableView);
	}
	
	// 開啟Window視窗
	self.open();
})();
